$(document).ready(function(){
    $(".slider").slick({
        dots: false,
        infinite: true,
        prevArrow: '<button type="button" class="button_prev"><img src="./img/layout/left_arrow.png" alt=""></button>',
        nextArrow: '<button type="button" class="button_next"><img src="./img/layout/right_arrow.png" alt=""></button>',
        responsive: [
            {
                breakpoint: 738,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });

    $(".scrollto").click(function(e) {
        e.preventDefault();
        console.log(this);
        $('html, body').animate({
            scrollTop: $(".partenaires").offset().top
        }, 500);
    });
});